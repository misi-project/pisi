# -*- coding: utf-8 -*-
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# Please read the COPYING file.

# ActionsAPI Modules
from pisi.actionsapi import get
from pisi.actionsapi import cmaketools
from pisi.actionsapi import shelltools

basename = "kde5"

prefix = "/%s" % get.defaultprefixDIR()


def configure(parameters = '', installPrefix = prefix, sourceDir = '..'):
    ''' parameters -DLIB_INSTALL_DIR="hede" -DSOMETHING_USEFUL=1'''

    shelltools.makedirs("build")
    shelltools.cd("build")

    cmaketools.configure("-DCMAKE_BUILD_TYPE=Release \
			            -DCMAKE_INSTALL_PREFIX=/usr \
			            -DLIB_INSTALL_DIR=lib \
			            -DLIBEXEC_INSTALL_DIR=lib \
			            -DBUILD_TESTING=OFF -Wno-dev", sourceDir="..")

    shelltools.cd("..")

def make(parameters = ''):
    cmaketools.make('-C build %s' % parameters)

def install(parameters = '', argument = 'install'):
    cmaketools.install('-C build %s' % parameters, argument)
